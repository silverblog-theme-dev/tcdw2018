import postcss from 'rollup-plugin-postcss';
import { terser } from 'rollup-plugin-terser';
import autoprefixer from 'autoprefixer';
import clean from 'postcss-clean';

const isDist = process.env.BUILD === 'production';

const buildPlan = {
    input: 'src/js/index.js',
    output: {
        file: 'dist/static/bundle.js',
        format: 'iife',
        sourcemap: !isDist,
    },
    plugins: [
        postcss({
            extract: true,
            sourceMap: !isDist,
            plugins: [
                autoprefixer(),
                clean(),
            ],
        }),
        terser(),
    ],
};

export default buildPlan;
