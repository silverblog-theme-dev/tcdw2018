import '../scss/index.scss';

const dom = (e) => document.querySelector(e);
const create = (tagName, attribute = {}, subDOMs = []) => {
    const temp = document.createElement(tagName);
    const attrName = Object.keys(attribute);
    for (let i = 0; i < attrName.length; i += 1) {
        temp.setAttribute(attrName[i], attribute[attrName[i]]);
    }
    for (let i = 0; i < subDOMs.length; i += 1) {
        temp.appendChild(subDOMs[i]);
    }
    return temp;
};

// 1. 浏览器 Hack
// 由于在非 webkit 系浏览器，使用圆角代码框配合没有圆角的滚动条会很难看，所以干脆去掉圆角（
if (!('WebkitAppearance' in document.documentElement.style)) {
    const CSSElement = document.createElement('style');
    CSSElement.textContent = 'main article .content pre code, .hljs {'
            + 'border-radius: 0;'
            + '}';
    document.head.appendChild(CSSElement);
}

// 2. 手机菜单
const overlay = create('div');
const mobileMenu = dom('nav');
const mobileMenuBtn = dom('#mobile-menu-btn');
const toggleMobileMenu = () => {
    overlay.classList.toggle('active');
    mobileMenu.classList.toggle('active');
};
overlay.addEventListener('click', toggleMobileMenu, false);
mobileMenuBtn.addEventListener('click', toggleMobileMenu, false);
overlay.className = 'overlay mobile-only';
document.body.appendChild(overlay);

// 3. 顶部菜单效果
const header = dom('header');
const homeImage = dom('.index-bg');
const navTopStyle = () => {
    const scroll = window.pageYOffset;
    if (scroll > homeImage.clientHeight / 2) {
        header.classList.remove('top');
    } else {
        header.classList.add('top');
    }
};
if (document.body.classList.contains('body-index')) {
    window.addEventListener('scroll', navTopStyle, false);
    navTopStyle();
}
